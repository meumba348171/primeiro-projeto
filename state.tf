terraform {
  backend "s3" {
    bucket = "base-config-rm348171"
    key    = "cicd"
    region = "us-east-1"
  }
}
